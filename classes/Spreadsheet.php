<?php defined('SYSPATH') or die('No direct script access.');

/**
 * PHPExcel Capsule
 */
class Spreadsheet {

  /**
   * @var PHPExcel
   */
  protected $_spreadsheet;

  /**
   * Creates spreadsheet with options
   *
   * @uses PHPExcel             PHPExcel
   * @uses PHPExcel_IOFactory   PHPExcel IO Factory
   *
   * @param string  $file       file to load
   * @param boolean $readonly   are we only going to read file?
   * @param mixed   $sheets     sheets to load
   *
   * @return Spreadsheet        Spreadsheet
   */
  public static function factory($file = NULL, $readonly = FALSE, $sheets = NULL)
  {
    return new Spreadsheet($file, $readonly, $sheets);
  }

  /**
   * Creates spreadsheet with options
   *
   * @uses PHPExcel             PHPExcel
   * @uses PHPExcel_IOFactory   PHPExcel IO Factory
   *
   * @param string  $file       file to load
   * @param boolean $readonly   are we only going to read file?
   * @param mixed   $sheets     sheets to load
   */
  public function __construct($file = NULL, $readonly = FALSE, $sheets = NULL)
  {
    ! defined('PHPEXCEL_ROOT') and Spreadsheet::init();

    // Return clean instance if there's no specified file
    if ($file === NULL)
    {
      $this->_spreadsheet = new PHPExcel;
      return;
    }

    // Create Reader
    $reader = PHPExcel_IOFactory::createReaderForFile($file);

    // Set read only for performance
    if ($readonly === TRUE)
    {
      $reader->setReadDataOnly(TRUE);
    }

    // Load Only sheets
    if ($sheets !== NULL)
    {
      $reader->setLoadSheetsOnly($sheets);
    }

    $this->_spreadsheet = $reader->load($file);
  }

  /**
   * Returns PHPExcel instance
   *
   * @return  PHPExcel  PHPExcel instance
   */
  public function getObject()
  {
    return $this->_spreadsheet;
  }

  /**
   * Shortcut method
   *
   * @param  string   $filename   where we want to save the file
   * @param  string   $writer     what writer we want to use
   */
  public function save($filename, $writer = 'Excel5')
  {
    PHPExcel_IOFactory::createWriter($this->_spreadsheet, $writer)
      ->save($filename);
  }

  /**
   * Imports dependencies
   */
  public static function init()
  {
    if ( ! defined('PHPEXCEL_ROOT'))
    {
      require Kohana::find_file('vendor/PHPExcel/Classes', 'PHPExcel');
    }
  }

}